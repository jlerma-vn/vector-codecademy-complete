console.log('arrays.js loaded');

            //0        /1      /2
let names = ['Joshua', 'Adam', 'Matt'];

console.log(names);

let firstEl = names[0];
let secondEl = names[1];
let thirdEl = names[2];

console.log(firstEl);
console.log(secondEl);
console.log(thirdEl);

let lengthOfArray = names.length;
console.log(lengthOfArray);

names.push('Jim');
console.log(names);
console.log(names.length);
names.pop();
console.log(names);
console.log(names.length);

let moreNames = ['Joshua', 'Adam', 'Lilly', ['Bill', 'Joe',['Chuck', 'Zack']]];

console.log(moreNames);

let chuck = moreNames[3][2][0];
console.log(chuck);