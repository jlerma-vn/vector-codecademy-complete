console.log('loaded objs..');

// object literal, 
let a = {
    keyOne: 'keyOne Value!',
    keyTwo: 'value of key two!',
    methodOne: function(first, last) {
        console.log(`Hello ${first} ${last}`);
    },
    obj: {
        names: ['Joshua', 'Danny', {
            final: 100
        }]
    }
};

console.log(a.keyOne);
console.log(a.keyTwo);

a.newProp = 'my new prop!';
console.log(a);
console.log(a.newProp);

a['my prop'] = 'another new prop!';
console.log(a);
console.log(a['my prop']);
a.methodOne('Joshua', 'Lerma');
console.log(a.obj.names[2].final);

let colors = {
    blue: '#0000ff',
    red: '#ff0000',
    green: '#00ff00',
    black: '#000000'
}

console.log(colors.blue);
console.log(colors.red);
console.log(colors.green);
console.log(colors.black);

for (const property in colors) {
    console.log(`Color: ${property} Hex Val Color: ${colors[property]}`);
}

// let blue = colors.blue;
// let red = colors.red;

// console.log(blue);
// console.log(red);

// object destructuring
let { blue, red } = colors;

console.log(blue);
console.log(red);