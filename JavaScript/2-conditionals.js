console.log('executing..');
if (true) {
    console.log('that is true!');
}

if (false) {
    console.log('this is not going to hit..');
}

if (false) {
    console.log('this will not hit..');
} else {
    console.log('this will hit because the expression resulted to false..');
}

if (false) {
    console.log('not hit..');
} else if (true) {
    console.log('this will hit..');
} else {
    console.log('within the else..');
}

// comparison operators, greater than
let a = 100;
let b = 200;
if (a == b) {
    console.log('that is true..');
} else if (a != b) {
    console.log('that is true! a is not equal to b..');
} else {
    console.log('that is false..');
}

let x = 300;
let y = 400;

if (x != y && y > x) {
    console.log('both expressions were true!');
}

if (x == y || y > x) {
    console.log('right expression is true..');
}

let ternaryOperator = x == y ? 'true' : 'false';

console.log(ternaryOperator);

let lengthyName = '';
let checkNameLength = lengthyName.length ? 'that has value': 'that is an empty name..';
console.log(checkNameLength);
lengthyName = 'Charlotte';
checkNameLength = lengthyName.length ? 'that has value': 'that is an empty name..';
console.log(checkNameLength);
console.log(0 ? 'true' : 'false');

