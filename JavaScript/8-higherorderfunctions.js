function higherOrderFunction(func) {
    let name = 'Joshua'
    func(name);
    console.log('after func..');
}

higherOrderFunction((name) => console.log('executed callback!' + name));

// Array forEach, map, filter

const names = ['Joshua', 'Marwan', 'Danny', 'Bill', 'Jan'];

names.forEach(name => console.log(name));

const namesAsterisk = names.map(name => name = `*${name}`);

console.log(namesAsterisk);

const longNames = names.filter(name => name.length > 4);

console.log(longNames);


