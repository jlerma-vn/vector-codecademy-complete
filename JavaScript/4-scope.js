console.log('scope.js loaded..');
let z = 'global variable..';

function myFunc() {
    let x = 100;
    console.log(z);
    console.log(x);
}

function secondFunc() {
    let y = 200;
    // console.log(x);
}

myFunc();
secondFunc();

// console.log(y);

function thirdFunction() {
    let firstVar = 'firstVar..';

    let funcExpression = function() {
        let secondVar = 'secondVar';
        console.log(firstVar);
    }

    funcExpression();

    console.log(secondVar);
}

thirdFunction();
