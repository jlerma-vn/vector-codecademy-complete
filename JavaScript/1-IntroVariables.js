/*
// First console statement
console.log('Hello World');

// Exploring Data types
console.log('Another console..');
console.log(100);
console.log(true);
console.log(undefined);
console.log(null);

// Arithmetic operators
console.log(100 + 20);
console.log(100 * 20);
console.log(100 / 20);
console.log(100 - 20);

// string concatenation
console.log('Hello'+ ' ' + 'Joshua');
console.log('HelloJoshua');
console.log('Hello' - 'World');

// String properties
console.log('Testing'.length); . (dot) is also known as a opertor
console.log('Testing'.toUpperCase());
console.log(' space between me...        ');
console.log(' space between me...        '.trim());
*/

// Create Variables: var, let, const
let x = 100;
let y = 200;
console.log(x);
console.log(x + y);
let z = 300;
console.log(z);
z = 400;
console.log(z);
let name = 'Joshua';
console.log(name);
name = 'Bob';
console.log(name);
let country;
country = 'USA';
console.log(country);

const lastName = 'Lerma';
console.log(lastName);
const fullName = 'Joshua Lerma';
console.log(fullName);

// add, subtract, multiply, divide..
let age = 35;
console.log(age);
age += 5;
console.log(age);
let budget = 10;
console.log(budget);
budget *= 1000;
console.log(budget);
let price = 300;
price -= 100;
console.log(200);
let total = 500;
total /= 2;
console.log(total);
let fName = 'Joshua';
let lName = 'Lerma';
let completeName = fName + ' ' + lName;
let stringInterpolation = `${fName} ${lName}`;
console.log(stringInterpolation);
console.log(completeName);
console.log(typeof fName);
console.log(typeof price);
// lastName = 'Changed..';
// console.log(lastName);
