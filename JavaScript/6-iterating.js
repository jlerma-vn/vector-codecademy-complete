console.log('iterating.js loaded..');

let companies = ['Microsoft', 'Apple', 'Verizon', 'Samsung'];
let names = ['Josh', 'Adam', 'Bill', 'John', 'Danny'];

console.log(companies[0]);
console.log(companies[1]);
console.log(companies[2]);
console.log(companies[3]);

// incrementers and simple loop..
for(let i = 0; i < 8; i++) {
    console.log('test');
    console.log(i);
}

function loopThroughArray(arr = []) {
    for (let i = 0; i < arr.length; i++) {
        console.log(arr[i]);
    }
};

loopThroughArray(companies);
loopThroughArray(names);

function whileThroughArray(arr = []) {
    let i = 0;
    while(arr.length > i) {
        console.log(arr[i]);
        i++
    }
};

whileThroughArray(companies);

function doWhileThroughArray(arr = []) {
    let i = 0;
    do {
        console.log('executed at least once!');
        console.log(arr[i]);
        i++
    } while(arr.length == i)
};

doWhileThroughArray(companies);
