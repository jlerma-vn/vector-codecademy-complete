function myFunc() {
    console.log('executed within myFunc!');
}

myFunc();

function myFuncWithParams(firstName = 'Default First', lastName = 'Default Last') {
    console.log(`Hello: ${firstName} ${lastName}`);
}

myFuncWithParams('Joshua', 'Lerma');
myFuncWithParams('Billy', 'Bob');
myFuncWithParams();

function returnName(name = 'Default First Name') {
    return name;
}

let myFirstName = returnName('Different Name');

console.log(myFirstName);

function calcDiameter(radius = 0) {
    if (typeof radius == 'number' && radius) {
        return radius * 2;
    } else {
        return 'given value is not a number! Unable to calculate radius';
    }
}

let calcTestOne = calcDiameter();
console.log(calcTestOne);
let calcTestTwo = calcDiameter(10);
console.log(calcTestTwo);

function calcCircumference() {
    let radius = 5;
    let diameter = calcDiameter(radius);
    let pi = 3.14;
    let circumference = 2 * pi * radius;
    return `Radius: ${radius} Diameter: ${diameter} Circumference: ${circumference}`;
}

console.log(calcCircumference());

let funcExpression = function() {
    console.log('func expression..');
};

funcExpression();

let arrow = () => {
    return 'arrow';
}

let result = arrow();
console.log(result);

// one arg with ()
// let arrowTwo = (a) => {
//     return `Arrow: ${a}`;
// }

let arrowTwo = a => {
    return `Arrow: ${a}`;
}

// let arrowTwo = (a, b) => { need () for more then 1 arg..
//     return `Arrow: ${a}`;
// }

let resultTwo = arrowTwo('Josh');
console.log(resultTwo);


let arrowThree = a => `Arrow: ${a}`;

let resultThree = arrowThree('Josh');
console.log(resultThree);
